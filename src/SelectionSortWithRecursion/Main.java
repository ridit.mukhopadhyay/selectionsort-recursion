package SelectionSortWithRecursion;
public class Main {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] arr = {2,1,3,4};
		int n = arr.length;
		displayArr(selectionSort(0, arr, n));
	}
	public static int[] selectionSort(int n,int[] arr,int length) {
		int small = arr[n];
		int indexToBeReplaced = 0;
		for(int i = n;i<length;i++) {
			if(arr[i] < small) {
				small = arr[i];
				indexToBeReplaced = i;
			}
		}
		if(arr[n] > small) {
			arr[indexToBeReplaced] = arr[n];
			arr[n] = small;
		}
		if(n == length-1) {
			return arr;
		}
		else {
			return selectionSort(n+1, arr, length);
		}
	}
	public static void displayArr(int[] arr) {
		int length = arr.length;
		for(int i = 0;i<length;i++) {
			if(i == length-1) {
				System.out.print(arr[i]);
			}
			else {
				System.out.print(arr[i] + ",");
			}
		}
	}
}
